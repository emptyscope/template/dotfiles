# dotfiles

## Computer Setup For Dotnet Core

Make sure you have dotnet core installed. [https://dotnet.microsoft.com/download/dotnet-core/2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2)

After you have installed dotnet core open a terminal window and run the following command:
```
dotnet tool install -g dotnet-format
```
More information about the formating tool can be found here: [https://github.com/dotnet/format](https://github.com/dotnet/format)


### Mac

You will need to install the nuget password manager.  The script below will set it up for you.

Right click and save the script to your computer: [https://raw.githubusercontent.com/microsoft/artifacts-credprovider/master/helpers/installcredprovider.sh](https://raw.githubusercontent.com/microsoft/artifacts-credprovider/master/helpers/installcredprovider.sh)

Once the script is downloaded run these commands on it:
```
chmod +X installcredprovider.sh
```

```
. installcredprovider.sh
```

You will need these environment variables added to your `.bash_profile`

Open a terminal window to your home directory (should be the default one that opens) and run:

```
nano .bash_profile
```

Add the following lines, replacing the items in `{}` with your information:

```
export ASPNETCORE_ENVIRONMENT=Local
export NuGet_Username={your_username}
export NuGet_Password={your_password}
export NuGet_Url={your_nuget_url}
export DatasourceUrl={your_database_url}
export DatasourceUserId={your_database_url}
export DatasourcePassword={your_database_url}
```

Save and close your `.bash_profile` commands to do that are as follows:
```
CTRL + X
ENTER
```

Reload your profile so the new environment variables are available by running this command:
```
. ~/.bash_profile
```

## Potential Issues
If build fails with a message containing "current .NET SDK does not support targeting .NET Core 2.2" then run `dotnet --version` in your terminal and copy the entire version. 

Then open global.json in the projects parent directory and change the version to the copied version.

### Windows

You will need to install the nuget password manager.  The script below will set it up for you.

Right click and save the script to your computer: [https://raw.githubusercontent.com/microsoft/artifacts-credprovider/master/helpers/installcredprovider.ps1](https://raw.githubusercontent.com/microsoft/artifacts-credprovider/master/helpers/installcredprovider.ps1)

Once the script is downloaded run these commands on it:
```
Unblock-File -Path .\installcredprovider.ps1
```

```
.\installcredprovider.sh
```

You will need these environment variables added to your system:

```
ASPNETCORE_ENVIRONMENT=Local
NuGet_Username={your_username}
NuGet_Password={your_password}
NuGet_Url={your_nuget_url}
DatasourceUrl={your_database_url}
DatasourceUserId={your_database_url}
DatasourcePassword={your_database_url}
```
