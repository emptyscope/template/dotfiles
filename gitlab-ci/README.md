# dotfiles

## Gitlab.com Setup

There is a gitlab folder that can be used to create new gitlab repositories with the default files that it needs based on the project type.

Make sure the environment variables in the table below are setup.

| Variable Name                     | Variable Value                       | State        |
| --------------------------------- | ------------------------------------ | -------------|
| DOCKER_REGISTRY                   | {docker.io}                          |              |
| DOCKER_REGISTRY_USER 	            | {dockerhub_username}        		   |              |
| DOCKER_REGISTRY_PASSWORD          | {dockerhub_password}                 | Protected    |
| GITLAB_ACCESS_TOKEN  	            | {gitlab_token}                       | Protected    |
| DOCKER_AUTH_CONFIG                | {docker_login_information}           | Protected    |
| VSS_NUGET_EXTERNAL_FEED_ENDPOINTS | {azure_artifact_server_information}  | Protected    |


Ideally the variables above are setup at the group level, but you can set them up at each repository to override the group ones if nessecary.


## Project Setup

* Create a new empty repository in gitlab through the web gui
* In the web gui click the create new file button
* Select a file type of .gitlab-ci.yml in the dropdown
* Select any .gitlab-ci.yml template so the file is named and pre-populated
* Override the pre-populated data with the information from below or from [https://gitlab.com/emptyscope/template/dotfiles/raw/main/gitlab-ci/.default-ci.yml](https://gitlab.com/emptyscope/template/dotfiles/raw/main/gitlab-ci/.default-ci.yml)

```
include:
  - project: 'emptyscope/template/dotfiles'
    file: '/gitlab-ci/.setup-ci.yml'
```
* In your commit message type `setup-` followed by one of the available setup examples:
    - setup-blank
    - setup-dotnet
    - setup-js
    - etc

* Once the build process completes a new `.gitlab-ci.yml` will be in the current repository
* Edit the `.gitlab-ci.yml` through the web gui replacing the required variables with your intended ones
    - values at the top of the file for solution and project naming
    - values in the deployment sections which are the name of the app service or function Azure resource where the code will be deployed. Only one will be needed which means you can ignore the other. For example, an API project will be deployed to an app service so there is no need to update the deploy function sections as they will not be needed.
* In the commit message start the message with the second half of your initial commit message
    - if the initial commit message was `setup-dotnet` then this new commit message will start with `dotnet-`
* The second half of this new commit message will depend on the type of project you created.
    - if the project only has one type then it doesn't matter what you put after it `dotnet-{whatever}`
    - if the project has subtypes you can fill those in. For example `dotnet-web` or`dotnet-nuget`

## Forked Project Setup

Once you fork a project it will need to be able to talk to our private docker registry.  In order for it to do that you will at minimum need the below environment variable setup for each project.


| Variable Name                     | Variable Value                       | State        |
| --------------------------------- | ------------------------------------ | -------------|
| DOCKER_AUTH_CONFIG                | {docker_login_information}           | Protected    |


The {docker_login_information} should look similar to this:

```
{
    "auths": {
        "registry.azurecr.io": {
            "auth": "QWxhZGRpbjpPcGVuU2VzYW1l"
        }
    }
}
```

Make sure to mark the `DOCKER_AUTH_CONFIG` environment variable as `Protected` within Gitlab.
